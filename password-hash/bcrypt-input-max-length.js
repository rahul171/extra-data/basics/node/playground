const bcrypt = require('bcrypt');

// const salt = bcrypt.genSaltSync(10);
//
// console.log(salt);

// $2b$10$MIS79rvlmj7p9kxx4JlBwe

const getRandomNumber = (a = 0, b = 1) => {
  return Math.floor(Math.random() * (b - a + 1)) + a;
}

const generateRandomString = (length) => {
  const pickFrom = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  let str = '';
  for (let i = 0; i < length; i++) {
    str += pickFrom[getRandomNumber(0, pickFrom.length - 1)];
  }
  return str;
}

const salt = '$2b$10$MIS79rvlmj7p9kxx4JlBwe';

// length > 72 will result in same hash.
const password = generateRandomString(72);

console.log(password);

const extra = 'h';

console.log(password.length);

const finalPwd = password + extra;

// const hash = bcrypt.hashSync(password, salt);
//
// const hash2 = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
//
// console.log(hash);
// console.log(hash2);
// console.log(bcrypt.compareSync(password, hash))
// console.log(bcrypt.compareSync(password, hash2))

const hash = bcrypt.hashSync(password, salt);
const hash2 = bcrypt.hashSync(finalPwd, salt);
const hash3 = bcrypt.hashSync(finalPwd + generateRandomString(100), bcrypt.genSaltSync(10));

console.log(bcrypt.compareSync(password, hash3));

console.log(hash);
console.log(hash2);
console.log(hash2.length);
console.log(hash === hash2);
