const crypto = require('crypto');
const bcrypt = require('bcrypt');

const getRandomNumber = (a = 0, b = 1) => {
  return Math.floor(Math.random() * (b - a + 1)) + a;
}

const generateRandomString = (length) => {
  const pickFrom = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  let str = '';
  for (let i = 0; i < length; i++) {
    str += pickFrom[getRandomNumber(0, pickFrom.length - 1)];
  }
  return str;
}

const password = generateRandomString(4000);
const password2 = password.slice(0, password.length - 2);
const password3 = generateRandomString(20);

console.log(password);
console.log(password2);
console.log(password2);

const sh256Hash = crypto.createHash('sha256').update(password).digest('hex');
const sh256Hash2 = crypto.createHash('sha256').update(password2).digest('hex');
const sh256Hash3 = crypto.createHash('sha256').update(password3).digest('hex');

console.log(sh256Hash);
console.log(sh256Hash2);
console.log(sh256Hash3);
