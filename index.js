test() {
    // const a = {a: 1, b: 2};
    // const b = Object.assign({}, a, {b: 3});
    // console.log(b);

    function A() {
        this.a = 1;
        this.b = 2;
    }

    const aa = new A();
    // comment this and see the results.
    delete aa.a;
    console.log(aa);
    const bb = Object.assign({a:10}, aa, {bb:3, c:4});
    console.log(bb);
}

test();
